class AddMovieIdToReservations < ActiveRecord::Migration[6.1]
  def change
    add_column :reservations, :movie_id, :integer
  end
end
