class AddSeatNoToMovies < ActiveRecord::Migration[6.1]
  def change
    add_column :movies, :seat_no, :integer
  end
end
