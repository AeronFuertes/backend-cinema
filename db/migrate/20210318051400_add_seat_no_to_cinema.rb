class AddSeatNoToCinema < ActiveRecord::Migration[6.1]
  def change
    add_column :cinemas, :seat_no, :integer
  end
end
