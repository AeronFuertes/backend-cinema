class CreateReservations < ActiveRecord::Migration[6.1]
  def change
    create_table :reservations do |t|
      t.integer :user_id
      t.integer :screen_id
      t.boolean :reserved

      t.timestamps
    end
  end
end
