class CreateScreens < ActiveRecord::Migration[6.1]
  def change
    create_table :screens do |t|
      t.time :screening_start

      t.timestamps
    end
  end
end
