class AddColumnsToReservations < ActiveRecord::Migration[6.1]
  def change
    add_column :reservations, :seat_number, :string
    add_column :reservations, :time_slot, :string
  end
end
