class AddIdToScreen < ActiveRecord::Migration[6.1]
  def change
    add_column :screens, :movie_id, :integer
    add_column :screens, :cinema_id, :integer
  end
end
