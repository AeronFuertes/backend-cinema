class CreateSeatReserveds < ActiveRecord::Migration[6.1]
  def change
    create_table :seat_reserveds do |t|
      t.integer :seat_id, :primary_key
      t.integer :reservation_id
      t.integer :screen_id

      t.timestamps
    end
  end
end
