class User < ApplicationRecord
  has_many :reservations
  before_save { self.email = email.downcase }
  validates :name, presence: {strick: true},
             length: { minimum: 7, maximum: 50 }

  VALID_EMAIL_REGEX= /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true,
            length: { maximum:105},
            uniqueness: { case_sensitive: false},
            format: { with: VALID_EMAIL_REGEX}

  validates :contact, presence: true,
            length: { is: 11 },
            numericality: { only_integer: true },
            uniqueness: true
  validates :password, confirmation: {case_sensitive: true}

  has_secure_password
end
