class Reservation < ApplicationRecord
  belongs_to :user
  belongs_to :movie

  default_scope -> { order(created_at: :desc) }

  validates :seat_number, presence: true
  validates :time_slot, presence: true 
end
