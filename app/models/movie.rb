class Movie < ApplicationRecord
  has_many :reservations
  validates :title, presence: {message: "must give a movie title"},
                    length: {minimum: 10, maximum: 100}
  validates :genre, presence: {message: "must give a genre for this movie"}
  validates :duration_min, presence: true
  validates :description, presence: true, length: {minimum: 10, maximum: 500}
end
