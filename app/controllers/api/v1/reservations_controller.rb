class Api::V1::ReservationsController < ApplicationController
  before_action :set_reservation, only: [:show, :update, :destroy]
  before_action :set_movie, only: [:create]
  before_action :set_user, only: [:appointment]
  # GET /reservations
  def index
    @reservations = Reservation.all.order(created_at: :desc)

    render json: @reservations, include: %w[user movie]
  end

  # GET /reservations/1
  def show
    render json: @reservation, include: %w[user movie]
  end

  def appointment
    me = @user
    @appointment = Reservation.all.where(user_id: me)
    render json: @appointment, include: %w[user movie]
  end

  # POST /reservations
  def create
    if Movie.find_by_id(params[:reservation][:movie_id])
      @reservation = Reservation.new(reservation_params)

      if @reservation.save
        render json: @reservation, status: :created
      else
        render json: @reservation.errors, status: :unprocessable_entity
      end
    end
  end

  # PATCH/PUT /reservations/1
  def update
    if @reservation.update(reservation_params)
      render json: @reservation
    else
      render json: @reservation.errors, status: :unprocessable_entity
    end
  end

  # DELETE /reservations/1
  def destroy
    @reservation.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation
      @reservation = Reservation.find(params[:id])
    end

    def set_movie
      @movie = Movie.find(params[:id])
    end

    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def reservation_params
      params.require(:reservation).permit(:user_id, :movie_id, :reserved, :time_slot, :seat_number => [])
    end
end
