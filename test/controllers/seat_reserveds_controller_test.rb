require "test_helper"

class SeatReservedsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @seat_reserved = seat_reserveds(:one)
  end

  test "should get index" do
    get seat_reserveds_url, as: :json
    assert_response :success
  end

  test "should create seat_reserved" do
    assert_difference('SeatReserved.count') do
      post seat_reserveds_url, params: { seat_reserved: { reservation_id: @seat_reserved.reservation_id, screen_id: @seat_reserved.screen_id, seat_id: @seat_reserved.seat_id } }, as: :json
    end

    assert_response 201
  end

  test "should show seat_reserved" do
    get seat_reserved_url(@seat_reserved), as: :json
    assert_response :success
  end

  test "should update seat_reserved" do
    patch seat_reserved_url(@seat_reserved), params: { seat_reserved: { reservation_id: @seat_reserved.reservation_id, screen_id: @seat_reserved.screen_id, seat_id: @seat_reserved.seat_id } }, as: :json
    assert_response 200
  end

  test "should destroy seat_reserved" do
    assert_difference('SeatReserved.count', -1) do
      delete seat_reserved_url(@seat_reserved), as: :json
    end

    assert_response 204
  end
end
